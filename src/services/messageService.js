import callWebApi from '../helpers/webApiHelper';
import moment from "moment";

export const getMessages = async url => {
  const response = await callWebApi(url);
  return response.json();
}

export const getMessagesSortedByDate = async url => {
  const jsonMessages = await getMessages(url);
  return jsonMessages.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
}

export const sortMessagesPerDates = (messages, dates) => {
  if (messages.length === 0 || dates.length === 0) {
    return [];
  }
  const messagesPerDates = [];
  for (let i = 0; i < dates.length; i++) {
    const date = dates[i];
    const dateMessages = [];
    for (let j = 0; j < messages.length; j++) {
      const message = messages[j];
      if (date.isSame(moment(message.createdAt), 'day')) {
        dateMessages.push(message);
      }
    }
    messagesPerDates.push({
      date: date,
      messages: [...dateMessages]
    });
  }
  return messagesPerDates;
}
