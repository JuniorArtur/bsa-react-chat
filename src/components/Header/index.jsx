import React from 'react';

import './styles.css';
import {formatLastMessageDateTime} from "../../services/dateTimeSerive";

export const Header = ({usersCount, messagesCount, lastMessageDate}) => (
  <header className='header'>
    <h3 className='header__title'>My chat</h3>
    <div className='header__participants'>
      <span><strong>{usersCount}</strong> participants</span>
    </div>
    <div className='header__msg-count'>
      <span><strong>{messagesCount}</strong> messages</span>
    </div>
    <div className='header__last-msg'>
      <span>last message at {formatLastMessageDateTime(lastMessageDate)}</span>
    </div>
  </header>
);

export default Header;
