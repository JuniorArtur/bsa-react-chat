import React from 'react';
import {Button, Icon, TextArea} from 'semantic-ui-react';
import uuid from 'uuid';
import moment from "moment";

import './styles.css';

export default class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageBody: ''
        }
    }

    handleMessageSend(e) {
        const {messages} = this.props;
        const {messageBody} = this.state;
        if (messageBody.trim().length > 0) {
            const newMessage = {
                id: uuid(),
                user: 'Artur',
                avatar: null,
                text: this.state.messageBody,
                createdAt: moment().toISOString(true),
                editedAt: null
            }
            messages.push(newMessage);
            this.props.updateMainState(messages);
            this.setState({messageBody: ''});
        }
    }

    render() {
        return (
          <div className='field message-input'>
              <TextArea
                placeholder='Message'
                value={this.state.messageBody}
                className='message-input__field'
                onChange={e => this.setState({messageBody: e.target.value})}
              />
              <Button
                className='message-input__btn'
                size='huge'
                labelPosition='right'
                onClick={e => this.handleMessageSend(e)}
              >
                  <Icon name='location arrow'/> Send
              </Button>
          </div>
        );
    }
}
