import React from 'react';
import {Button, Dropdown, Feed, Form, Icon, Modal, TextArea} from 'semantic-ui-react';
import moment from 'moment';

import './styles.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            messageBody: this.props.message.text,
            author: this.props.author,
            isLiked: false,
            isHovering: false
        }
        this.handleMouseHover = this.handleMouseHover.bind(this);
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
            isHovering: !state.isHovering,
        };
    }

    handleEditing(e) {
        const {message, messages, updateMainState} = this.props;
        const {messageBody} = this.state;
        if (messageBody.trim().length > 0) {
            const index = messages.findIndex(m => m.id === message.id);
            const newMessages = Object.assign([], messages, {
                [index]: {
                    id: message.id,
                    user: message.user,
                    avatar: null,
                    text: messageBody,
                    createdAt: moment().toISOString(true),
                    editedAt: moment().toISOString(true)
                }
            });
            updateMainState(newMessages);
            this.setState({isModalOpen: false, isHovering: !this.state.isHovering});
        }
    }

    handleDeletion(e) {
        const {message, messages, updateMainState} = this.props;
        const newMessages = messages.filter(m => m.id !== message.id);
        updateMainState(newMessages);
    }

    render() {
        const {message, author} = this.props;
        const {isModalOpen, isLiked, isHovering} = this.state;
        const {user, avatar, createdAt, text} = message;
        const date = moment(createdAt).fromNow();
        const editModal = (
          <Modal
            closeIcon
            dimmer='blurring'
            centered={false}
            open={isModalOpen}
            onClose={() => this.setState({isModalOpen: false, isHovering: !isHovering})}
          >
              <Modal.Header>Edit message</Modal.Header>
              <Modal.Content>
                  <Form onSubmit={e => this.handleEditing(e)}>
                      <Form.Group inline>
                          <Form.Field
                            control={TextArea}
                            placeholder='Message'
                            onChange={e => this.setState({messageBody: e.target.value})}
                            value={this.state.messageBody}
                          />
                          <Button color='vk'>Edit</Button>
                      </Form.Group>
                  </Form>
              </Modal.Content>
          </Modal>
        );
        return (
          <div
            className='message'
            onMouseEnter={this.handleMouseHover}
            onMouseLeave={this.handleMouseHover}
          >
              {editModal}
              <Feed>
                  <Feed.Event>
                      {avatar &&
                      <Feed.Label>
                          <img src={avatar} alt='avatar'/>
                      </Feed.Label>
                      }
                      <Feed.Content>
                          <Feed.Summary>
                              <Feed.User> {user} </Feed.User>
                              <Feed.Date> {date} </Feed.Date>
                              {user === author && isHovering && (
                                <Dropdown
                                  icon='cog'
                                  inline
                                  className='ui right floated message__options'
                                >
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={() => this.setState({isModalOpen: true})}>
                                            <Icon name='edit'/> Edit
                                        </Dropdown.Item>
                                        <Dropdown.Item onClick={e => this.handleDeletion(e)}>
                                            <Icon name='trash'/> Delete
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                              )}
                          </Feed.Summary>
                          <Feed.Extra text> {text} </Feed.Extra>
                          <Feed.Meta>
                              <Feed.Like onClick={() => this.setState({isLiked: !isLiked})}>
                                  <Icon name='like'/>
                                  {isLiked && user !== author ? 1 : 0} likes
                              </Feed.Like>
                          </Feed.Meta>
                      </Feed.Content>
                  </Feed.Event>
              </Feed>
          </div>
        );
    }
}

export default Message;
