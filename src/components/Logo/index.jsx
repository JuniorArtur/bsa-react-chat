import React from 'react';

import './styles.css';

export const Logo = ({logo, title}) => (
  <div className='app__logo-container'>
    <div className='app__logo'>
      <img src={logo} className='app__logo-img' alt='logo'/>
    </div>
    <h2 className='app__title'>{title}</h2>
  </div>
);

export default Logo;
