import React from 'react';
import Message from '../Message';
import {sortMessagesPerDates} from "../../services/messageService";
import {formatMessagesDivisorDate} from "../../services/dateTimeSerive";

import './styles.css';
import '../Message/styles.css';

export const MessageList = ({currentUser, messages, dates, updateMainState}) => {
      const messagesByDates = sortMessagesPerDates(messages, dates);
      return (
        <>{
              messagesByDates.map(dateMessages =>
                <div>
                      <div className='message-divider'>
                            <span className='message-divider__line'/>
                            <p className='message-divider__date'>{formatMessagesDivisorDate(dateMessages.date)}</p>
                            <span className='message-divider__line'/>
                      </div>
                      {
                            dateMessages.messages.map(message =>
                              <ul className='message-list'>
                                    <li className=
                                          {(
                                            message.user === currentUser
                                              ? 'message message--out'
                                              : 'message message--in'
                                          )}
                                    >
                                          <Message
                                            key={message.id}
                                            message={message}
                                            messages={messages}
                                            author={currentUser}
                                            updateMainState={updateMainState}
                                          />
                                    </li>
                              </ul>
                            )
                      }
                </div>
              )
        }</>
      )
};

export default MessageList;
