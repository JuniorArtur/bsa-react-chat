import React from 'react';
import { render } from 'react-dom';
import Chat from './containers/Chat';

import 'semantic-ui-css/semantic.min.css';
import './styles/common.css';

render(
  <React.StrictMode>
    <Chat />
  </React.StrictMode>,
  document.getElementById('root')
);
