import React from 'react';
import Spinner from '../../components/Spinner';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import moment from 'moment';
import {Menu} from 'semantic-ui-react';
import {getMessagesSortedByDate} from "../../services/messageService";

import logo from './chat-logo.svg';
import './styles.css';

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isLoading: true,
      chatOwner: {
        name: 'Artur'
      }
    }
    this.updateMainState = this.handler.bind(this);
  }

  async componentDidMount() {
    const sortedByDateMessages = await getMessagesSortedByDate(
      'https://edikdolynskyi.github.io/react_sources/messages.json'
    );
    this.setState({
      messages: sortedByDateMessages,
      isLoading: false
    });
  }

  handler(messages) {
    this.setState({
      messages
    });
  }

  render() {
    const {messages, isLoading, chatOwner} = this.state;
    const logoTitle = 'Chatify';
    let usersCount = 0;
    let uniqueSortedDates = [];
    let lastMessageDate = '';
    const messagesCount = messages.length;
    if (messagesCount > 0) {
      usersCount = [...new Set(messages.map(m => m.user))].length + 1;
      const strDates = messages.map(m => m.createdAt.substring(0, 10));
      uniqueSortedDates = [...new Set(strDates)].map(d => moment(d));
      lastMessageDate = messages[messages.length - 1].createdAt;
    }
    return (
      isLoading
        ? <Spinner/>
        : (
          <div className='chat'>
            <Logo logo={logo} title={logoTitle}/>
            <Menu className='chat__header' borderless>
              <Menu.Item as='h1' header>
                <Header
                  usersCount={usersCount}
                  messagesCount={messagesCount}
                  lastMessageDate={lastMessageDate}
                />
              </Menu.Item>
            </Menu>
            <main className='chat__content'>
              <MessageList
                currentUser={chatOwner.name}
                messages={messages}
                dates={uniqueSortedDates}
                updateMainState={this.updateMainState}
              />
            </main>
            <Menu className='chat__footer' borderless>
              <Menu.Item header>
                <MessageInput
                  messages={messages}
                  updateMainState={this.updateMainState}
                />
              </Menu.Item>
            </Menu>
            <footer className='chat__footer-title'>
              <p className='chat-footer__text'>&copy; BSA Team</p>
            </footer>
          </div>
        )
    )
  }
}
